/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.shapeinheritance;

/**
 *
 * @author MSI GAMING
 */
public class Triangle extends Shape {
    private double b;
    private double h;
    public Triangle(double b,double h){
        this.b = b;
        this.h = h;
    }
    @Override
    public double calArea(){
        return   0.5 * b * h;
    }
    
    public double getOutput(){
        return b;
    }
    public double getOutput1(){
        return h;
    }
    @Override
    public void print(){
        System.out.println("Area of triangle(base = "+ this.getOutput1()+ ")(height = "+ this.getOutput()+ ") is " +this.calArea());
    }
}
