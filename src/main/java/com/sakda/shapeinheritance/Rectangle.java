/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.shapeinheritance;

/**
 *
 * @author MSI GAMING
 */
public class Rectangle extends Shape {

    protected double w;
    protected double l;
 
    public Rectangle(double w, double l) {
        this.w = w;
        this.l = l;
        
    }

    @Override
    public double calArea() {
        return w * l;
    }
    
    public double getOutput(){
        return w;
    }
    public double getOutput1(){
        return l;
    }
    @Override
    public void print(){
        System.out.println("Area of rectangle(width = "+ this.getOutput()+ ")(height = "+ this.getOutput1()+ ") is " +this.calArea());
    }
}
