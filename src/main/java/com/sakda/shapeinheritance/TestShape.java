/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.shapeinheritance;

/**
 *
 * @author MSI GAMING
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle = new Circle(3);
        circle.print();
        Triangle triangle = new Triangle(4,3);
        triangle.print();
        Rectangle rectangle = new Rectangle(3,4);
        rectangle.print();
        Square square = new Square(2);
        square.print();
        Circle circle1 = new Circle(4);
        circle1.print();
        
        Shape[] shapes = {circle,triangle,rectangle,square,circle1};
        for(int i=0; i<shapes.length; i++){
            shapes[i].print();
            
            }
        }
    }

